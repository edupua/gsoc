<div style="display: flex; justify-content: center;  align-items: center;">

  <img style="display: inline;" title="GSoC 2024" src="https://www.nexb.com/wp-content/uploads/2022/03/gsoc-sun-logo-new.png" width="321">

  <img title="libvirt" src="https://news-cdn.softpedia.com/images/news2/Libvirt-1-0-1-Released-Hundreds-of-Changes-Implemented-2.png" width="221" height="100">
</div>

<h1 align="center"> Google Summer of Code 2024 with <a href="https://libvirt.org"><span style="color:#57a7ff">Libvirt</span> </h1></a>

## Project Description

The project aimed to use fuzz-testing to find vulnerabilities in the
libvirt library. The virsh tools accept configuration inputs in XML
format, so fuzzing with XML-aware fuzzers can generate XML files to pass
into the library.

### Project Timeline and Storyline (How it turned out)

#### June 1st - 20th

In the first few weeks, I spent time learning about fuzzing in general,
studying popular oss-fuzz projects, but none of them were related to
XML.

But I wasn't in much communication with my mentor at this stage which he
expressed near the end of the month. I was trying to gain experience by
fuzzing random software, but I didn't realize how vast and uncorrelated
different fuzzing techniques were.

It would have been more beneficial if I spent more time looking through
the libvirt code and learn to pass the XML input.

#### June 20th - July 10th

In the next few weeks, I put in a lot of effort trying different fuzzing
methods on a trial-and-error basis:

- My first approach was to directly fuzz the virsh binary with
  instrumentation, but virsh was too large and the execution speed was
  very slow.

- I then learned about persistent mode and implemented it, but
  encountered limitations like not being able to fuzz binaries with
  repeated fork() calls or different initializer layers. I tried using a
  tool called preeny to stub out these calls, but that made the virsh
  binary unstable.

- Finally, I took advice from oss-fuzz projects and created custom
  harnesses - small pieces of code like unit tests that could be used
  for fuzzing. I was initially hesitant because I'd have to create a
  separate harness for each virsh subcommand, and was worried about not
  fuzzing the driver code leading up to the XML parsing. After
  discussing with my mentor, I got a clearer idea of which parts of the
  source code to actually fuzz.

I ended up writing harnesses for some of the common XML parsing functions 
used in libvirt.

#### July 10th - August 1st

In the final stage, I focused on making my setup more reproducible so
others could take up fuzzing libvirt. I provided various script files
for building, gathering coverage, using custom mutators, generating fuzz
campaigns with different options, etc. The end result is a well-refined
setup with detailed documentation, available in [this gitlab repo](https://gitlab.com/edupua/libvirt-aflfuzz).

This effort was motivated by my mentor, who wanted to run the fuzzers on
a high-performance machine. It required a lot of trial and error from
both of us to get to a working state. I really appreciate the active
communication and daily updates from my mentor, even when the current
efforts weren't helpful in finding serious bugs.

At this point, it became clear that the current fuzzing method was not
very effective. The fuzzers struggled after a day of fuzzing, and
theoretically, most of them were getting stuck at the XML validation
phase, where mutations would break the validation. After long periods of
fuzzing, the fuzzers would also start cherry-picking a few input files,
leading to minimal coverage improvement.

#### August 2nd - August 20th

My mentor started to show less interest in the current fuzzing setup, so
I had to explore more non-conventional approaches. I started by trying
to improve the corpus, as that seemed like an easier approach for me.

I tried various tools available on the internet to randomly generate XML
files, but most of them could barely handle enums, let alone complex
regular expressions. I eventually decided to create my own Python
script, called [lxmlgen](https://gitlab.com/edupua/lxmlgen), which uses
the lxml library to generate XML nodes. I made sure to not compromise on
even simple string manipulations and used libraries like rstr to handle
regex, along with the random library and some weighting.

The results for the storagevol specification (the simplest one) were
quite impressive - about 10,000 of these generated files had good
coverage, and using afl-cmin made it easier for the fuzzers to work with
them. This motivated me to write similar Python scripts for all the
subcommands, but the process was very time-consuming, even though I
found ways to semi-automate it.

With only two weeks left, I decided to write my own simple Python-based
mutator, with basic string manipulation and some hardcoded predictors
for simple enums like (yes/no) (on/off). This mutator seemed to work
fine and improved the coverage, but I haven't generated any reports for
longer runs yet.

## Progress

I have pushed all of my setup to [a repo](https://gitlab.com/edupua/libvirt-aflfuzz) 
which can be used to reproduce my fuzzing setup.
This includes the final versions of most of the tools and scripts
I have worked on.

## Pending tasks

I've implemented most of what I promised, even though I may have
underdelivered in some areas and was running behind schedule at times. I
can safely say I've explored a wide variety of possibilities and chosen
the right approaches. I've documented my experiences in [older
readme](https://gitlab.com/edupua/fuzz-testing) files, which can serve
as a cautionary tale for how not to fuzz a large library. There are
still plenty of areas for improvement:

- Add more complex mutation techniques and implement analyzer functions
  like introspect(), describe(), queue_new_entry(), etc. These would
  help analyze the actual progress of the mutator and improve the runs.

- **Use other fuzzers:** The ideal scenario would be to have OSS-Fuzz
  integration, which I thought of working on towards the end, but I had
  little to no knowledge about libFuzzer and writing harnesses for it. I
  understand that OSS-Fuzz uses a combination of different options, just
  like we've been doing for AFL. But the build system would need to be
  changed to use libFuzzer harnesses, and I'm willing to help with that.
  My build script can still be extended (or trimmed) to be used as an
  OSS-Fuzz build.sh, but adapting my Python mutator to fit libFuzzer may
  take more time.

- Integration into repo - Although I have mixed opinions about doing so,
  some of the components of the project can be integrated to the project
  maybe CI (but not likely not the ideal usecase). One thing is for sure
  is to make it such that anyone can take up fuzzing the project and
  detailed documentation on the procedure to do so has been documented.

## Experience

This project taught me a lot. It made me less negligent when committing
changes and more atomic in my approach, though I still struggle with
this at times. It also showed me the importance of thorough
documentation, and how unexpectedly time-consuming and effortful that
can be.

The project also gave me a good perspective on the inner workings of
organizations like this, in contrast to regular companies. It exposed me
to the struggles faced by the developers and board members, their
expectations, and how they treat new contributors. This was a valuable
learning experience.

While there is still room for improvement, I believe I delivered on most
of what I promised, and the experience has been invaluable.
