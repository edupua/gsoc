\documentclass[11pt,titlepage]{article}

\usepackage{graphicx}
\usepackage{amsmath,amsthm,amssymb,color,latexsym}
\usepackage{enumitem,cancel}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{tabularray}
\usepackage[numbers]{natbib}

\usepackage{tocloft}
\usepackage{blindtext}

\usepackage{glossaries}

\makenoidxglossaries


\newglossaryentry{mutation}
{
    name=mutation,
    description={A mutation in this context is a simple string manipulation -
    say, inserting a (random) character, deleting a character, or flipping a bit
in a character representation}
    plural=mutations
}


\newglossaryentry{parallel}
{
    name=parallel runs,
    description={Parallel runs in fuzzing involve running multiple instances of
    the fuzzer concurrently on different cores, machines, or even in distributed
environments. Usually the file corpus is automatically shared and improved between all
fuzzed processes.}
}

\newglossaryentry{corpus}
{
    name=seed corpus,
    description={A seed corpus refers to the initial set of input files or data
    that are used to start the fuzzing process. These input files can be either
generated manually or collected from real-world applications or inputs. }
}

\newglossaryentry{dictionary}
{
    name=dictionary,
    description={
        A fuzzing dictionary can be used to guide the fuzzer, and teach basic
        grammar of the file input. Dictionaries can be of type JSON, XML, PNG.
    }
    plural=dictionaries
}

\newglossaryentry{instrumentation}
{
    name=instrumentation,
    description={
        Instrumentation is a technique to measure code coverage by adding markers to
        code blocks.
    }
}

\newglossaryentry{harness}
{
    name=harness,
    description={A fuzzing harness is developed to bridge the gap between how the fuzzer
    expects input to occur and how input actually happens in the application.
    It does this by carrying the input from the fuzzer and delivering it
    properly to the fuzzing target so that the target can process the input
    like any normal interaction.
    }
}

\setcounter{tocdepth}{3}


\tolerance 99999
\hbadness 99999

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.99,0.99,0.97}
\definecolor{darkgreen}{rgb}{0.40,0.67,0.47}


\hypersetup{
    colorlinks=true,
    linkcolor=darkgreen,
    filecolor=pink,
    urlcolor=blue,
    pdftitle={Overleaf Example},
    pdfpagemode=FullScreen,
}



\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2
}

\lstset{style=mystyle}


% Margins
\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\newenvironment{points}{\vspace{0.3cm} \begin{enumerate}[label={(\alph*)}]}{\end{enumerate} \vspace{0.2cm}}


\title{ GSoC 2024 }
\author{ Abhiram Tilak (atp.exp@gmail.com) }
\date{\today}

\begin{document}


% custom title
\makeatletter
    \begin{titlepage}
        \begin{center}
            \includegraphics[width=3in]{assets/logo.png}\\[4ex]
            \vspace{2cm}
            {\huge \bfseries  \@title }\\[2ex]
            {\LARGE  \@author}\\[5ex]
            {\large \@date} \\ \vspace{3cm}
            \includegraphics[width=4in]{assets/gsoc.png}\\ \vspace{1cm}
        \end{center}
        { \textbf{\large{Contact info:}}
            \begin{itemize}
                \item Github:
                \href{https://github.com/abhiramtilakiiit}{abhiramtilakiiit}
            \item Gitlab: \href{https://gitlab.com/notpua/}{notpua}
               \item IRC: notpua
                \item Email: atp.exp@gmail.com
                \item Alternate email: atp@tutamail.com
            \end{itemize}
        }

    \end{titlepage}
\makeatother


% custom table of contents
% Redefinition of ToC command to get centered heading
\makeatletter
\renewcommand\tableofcontents{%
  \null\hfill\textbf{\Large\contentsname}\hfill\null\par
  \@mkboth{\MakeUppercase\contentsname}{\MakeUppercase\contentsname}%
  \@starttoc{toc}%
}
\makeatother


\thispagestyle{empty}
\newpage

\tableofcontents

\pagebreak

\section{Introduction}

\subsection{Synopsis}

The project that I chose was:
$$
\boxed{\text{ QEMU command line generator XML fuzzing}}
$$


This project is dedicated to the systematic execution of diverse fuzzing
techniques employing an array of specialized fuzzers. Its principal goal is the
detection of vulnerabilities within the libvirt binaries, particularly those
tasked with parsing XML input.

This project holds substantial significance due to the utility of
fuzzing being a prevalent approach for identifying vulnerabilities within the
cybersecurity domain.

\subsection{Importance of this project}

Over the past decade, a plethora of critical bugs like "Heartbleed" have
been unearthed and subsequently exploited through the application of fuzzing
techniques. Therefore, it is imperative for developers to integrate regular
fuzzing procedures into their workflows, facilitating the timely detection
and remediation of bugs before they escalate into significant security threats.

\subsection{Why I chose fuzzing}

While I'm familiar with fuzzing as a black box testing method, I'm still
relatively new to the domain and eager to delve deeper. I've come across
'wfuzz' in the past as one of the web fuzzing engines bundled with Kali
Linux.

But, fuzzing has evolved from black-box to white-box testing, spanning multiple
programming languages beyond C/C++. Statically linked libraries have become
prevalent in this evolution, amplifying fuzzing's effectiveness.

As a newcomer to the libvirt project, I prefer to work on a project that
doesn't require extensive examination of potentially less documented
source code. Given the availability of robust and well-documented fuzzing tools
in the market, which are based on black-box testing principles, leveraging such
tools would greatly facilitate my initial contributions.


\section{Current Progress}

\subsection{Getting Familiar with Libvirt}

During the project evaluation period, I dedicated significant time to immersing
myself in the operational aspects of projects like Libvirt and related tools.
After successfully compiling and testing Libvirt, I extended my efforts to
similar projects such as virt-manager, libvirt-python, libvirt-glib, and lcitool
(with Docker), to broaden my skill set.

Additionally, I initiated my involvement and submitted a few small patches.
One of these patches was a \texttt{BiteSizedTask} that I found in the
GitLab Issue Tracker, and was related to adding a new feature
'modify-or-add' to the \texttt{virsh net-update} sub-command
 \cite{net-update}. My other patch was
just a small version upgrade in \texttt{qcow2} \cite{qcow2}.

\subsection{Trial Fuzzing}

During the exploration phase of familiarizing myself with various fuzzing
techniques, I delved into projects listed on oss-fuzz's website, such as
\texttt{Wazuh},
\texttt{Pidgin}, and \texttt{html5-tidy}, which specifically target XML file fuzzing. I
supplemented my learning by watching video walk-through tutorials on fuzzing and
experimented with tools such as Honggfuzz and AFLPlusPlus, exploring sample
projects provided in their tutorials\cite{youtube}.

Subsequently, on gaining some confidence, I initiated fuzzing trials on binaries
like \texttt{virsh create},
\texttt{virsh define}, and \texttt{virsh net-create}, with ongoing efforts directed towards
documenting my experiences with these fuzzers, available in this
\href{https://gitlab.com/edupua/fuzz-testing}{Gitlab repo}.

\section{Tools Required For Fuzzing Libvirt}

\subsection{Fuzzing Engines}
As previously indicated, there exists a broad spectrum of fuzzing engines and services
accessible in the market for integration into our codebase. Below, I outline several
of them along with their respective advantages and disadvantages:


    \subsubsection{\href{https://llvm.org/docs/LibFuzzer.html}{libFuzzer}}

        LibFuzzer is an in-process, coverage-guided, fuzzing engine. It is
        maintained by the LLVM project, thus it is well-integrated with
        Clang.
        It is one of the easiest tools, to integrate fuzzing into any C/C++
        project. It relies on SanitizerCoverage (also part of LLVM)
        to guide corpus expansion and \glspl{mutation}. It supports user-supplied mutators which
        allows it to perform structure-aware fuzzing for any complex input type

        It supports \gls{corpus}, \gls{parallel} and \glspl{dictionary} too.

        \paragraph{Drawbacks: }
        A Possible disadvantage is that it only widely supports C/C++.
        There are other variants for Go, Rust and Swift
        but since most of Libvirt's code is in C, so shouldn't be an issue.

        A major drawback to LibFuzz is that most of its original developers
        moved to the Centipede (ML-based fuzzing), which is now a part of
        Google's \href{https://github.com/google/fuzztest}{Fuzztest}.

    \subsubsection{\href{https://aflplus.plus}{AFLPlusPlus:}}
        AFL++ is a superior fork of the original
        \href{https://lcamtuf.coredump.cx/afl/}{AFL} which was initially created because it was
        not maintained well since 2017.

        It is known for its effectiveness in discovering bugs, especially
        in complex programs, so a goto for binary-only fuzzing. AFL++ has a large
        user base and active community, which means good support and ongoing
        development.

        It can be instrumented into the source code and both software
        (Qemu-mode/Frida-mode) and
        hardware modes to fuzz the targets. It also supports \glspl{dictionary}
        and \gls{corpus} is a requirement.

        \paragraph{Drawbacks: }
        AFL++ can have significant performance overhead due to
        \gls{instrumentation}.

        As you can read through my trial experience, configuring AFL for optimal performance
        might require some expertise.

    \subsubsection{\href{https://github.com/google/honggfuzz}{Honggfuzz:}}

        Honggfuzz is a security-oriented, feedback-driven, easy-to-use fuzzer
        known for its interesting analysis options. It is maintained by
        Google, and also has software-emulator mode using QEMU.

        It’s capable of Parallel runs. Known for its hardware code tracking
        features like CPU: branch/instruction counting, Intel BTS, Intel PT).
        Also has a QEMU mode like AFL++ for software-based binary fuzzing.
        Also, it supports \glspl{dictionary} and file \gls{corpus} is necessary.

        \paragraph{Drawbacks:}
        The configuration might be a bit more complex compared to simple fuzzers.

    \subsection{Fuzzing as a service:}

    Once a project has been "fuzzed", it is important to keep track of all the
    bugs and vulnerabilities, and fuzz regularly to make sure the bugs are
    dealt with.


    While it is relatively straightforward to incorporate fuzzing into the
    continuous workflow of a project using tools like
    \href{https://www.code-intelligence.com/cli-tool}{ci-fuzz}, there are
    numerous services such as
    \href{https://github.com/google/oss-fuzz}{oss-fuzz} (available via repository) and
    \href{https://www.microsoft.com/en-us/research/project/project-springfield/}{MSRD}
    (accessible as a web application) that provide open-source project
    maintainers with the opportunity to include their projects for free.
    Additionally, these services may offer integration rewards as incentives for
    participation.

    Once the project is accepted, the infrastructure starts to run the fuzzer(s)
    and reports issues as and when they are found. As soon as the developer fixes the bug,
    the fix is automatically verified and the issue is closed. They also offer
    various tools for analysis.

    \subsection{Tools proposed to be used in Libvirt:}
    Each of the listed engines and services possesses its unique use cases
    and limitations. It has been extensively documented that relying solely on
    one specific fuzzer may not be sufficient, as each excels in detecting
    particular types of bugs. Consequently, we will adopt a strategy of
    utilizing a combination of all these fuzzers to maximize bug detection
    effectiveness.

    Every fuzzing engine has its own method for injecting \gls{instrumentation} into
    the target application. But the essential steps are the same. It requires
    the function \texttt{LLVMFuzzerTestOneInput()} to be implemented, and
    compiler variables to be set manually. The compiler then
    adds different functions on clearly defined positions like the beginning of
    a basic block or above an ‘if’-statement.

\section{Implementation Details}

By the culmination of this project, my objective is to conduct comprehensive
fuzzing of libvirt, while simultaneously refining the performance of the engine
to a level where it can effectively explore new paths and ideally uncover
previously unidentified bugs.

\subsection{Primary Goals}

These are the objectives I aim to achieve by the conclusion of the coding period
for GSoC. It's important to note that these sections are not presented in
chronological order, and any of these processes may be utilized throughout the
entirety of the project.

\subsubsection{Setting up a Fuzzing Target}

The first step in using any fuzzer on a library is to implement a fuzz target –
a function that accepts an array of bytes and does something interesting with
these bytes using the API under test.

Below is an example implementation of the \texttt{LLVMFuzzerTestOneInput}
function (cpp) which calls the XML Document parsing function. This example
was taken from the tinyxml2's fuzzing implementation from
\href{https://github.com/google/oss-fuzz/blob/2a7a5f37456a093e708dc3fa27bbad1a7511fd3f/projects/tinyxml2/xmltest.cpp}{oss-fuzz repo}.

\begin{lstlisting}[language=c++]
extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
	std::string data_string(reinterpret_cast<const char*>(data), size);
	XMLDocument doc;
	doc.Parse( data_string.c_str() );
	return 0;
}
\end{lstlisting}


In libvirt, we can call some of the functions that use the libXML's implementations
like \texttt{\href{https://github.com/libvirt/libvirt/blob/32d836bc6f8d6b76409c513ae27fedb984dd4fac/src/util/virxml.h\#L99}{virXMLNodeContentString()}}

As with any other brute-force tool, the fuzzer offers limited coverage if encryption,
checksums, cryptographic signatures, or compression are used to wholly wrap the actual
data format to be tested. Try to avoid making them a part of your fuzz target and replace
them with stubs.

\subsubsection{Compiler Setup}

This step involves selecting the right compiler and the
right sanitizers. Selection of the right compilers and sanitizers along with a myriad of
options requires heavy experimentation.

For Example:
AFL offers compilers like \texttt{afl-clang-lto}, \texttt{afl-clang-fast}, \texttt{afl-gcc-fast}, etc.

\begin{itemize}
	\item \textbf{Link Time Optimizations (LTO): }
	This is useful for reducing edge collisions in programs where finding new paths
	and reaching higher depths is important. However, instrumenting the link-time optimization
	can lead to slower build times. Libfuzzer has a similar implementation called PCGUARD
	for collision fuzzing.

	\item \textbf{Fast LLVM/GCC: }
        This uses true compiler-level \gls{instrumentation}, instead of the more crude assembly-level
rewriting approach taken by normal GCC and clang. These can be of great advantage for slower binaries.

\end{itemize}

Meanwhile, in Libfuzzer there is no installation needed, just use clang and add \texttt{fuzzer} as
an argument for \texttt{fsanitize}.

\begin{lstlisting}
clang -g -O1 -fsanitize=fuzzer mytarget.c
\end{lstlisting}



\paragraph{Using Sanitizers}

It is possible to use sanitizers when instrumenting targets for fuzzing, which
allows you to find bugs that would not necessarily result in a crash.

The sanitizers which are supported in most fuzz compilers are:
\begin{itemize}
    \item Address Sanitizers (ASAN)
    \item Memory Sanitizers (MSAN)
    \item Undefined Behaviour Sanitizers (UBSAN)
    \item Control Flow integrity Sanitizers (CFISAN)
    \item Thread Sanitizers (TSAN)
    \item Leak Sanitizers (LSAN)
\end{itemize}

The aim is to use all these sanitizers to examine possible violations,
but most of these sanitizers cannot be used together, so they need
experimentation with every combination.

\paragraph{Changing the build system}

The recommended way of passing the right compiler is to symlink the chosen
binary with a sample one. Example: afl-cc $\rightarrow$ afl-clang-fast

In meson, you just have to set the compiler in the very first command.
\begin{lstlisting}[language=bash]
CC=afl-cc CXX=afl-c++ meson
\end{lstlisting}

Finally, compile the target source code to get the \gls{harness}.
It is advised to always compile the programs statically and link these
to the target, and not use shared libraries.

\subsubsection{Generating corpus}

Most fuzzing engines are coverage-guided fuzzers and require some file corpus to start
with. This can be of any file type but for better path exploration it is good to
have the expected input, here XML.\cite{corpus}

Some basic techniques in generating a good corpus are:
\begin{itemize}
\item \textbf{Create them: } One of the best ways to generate good testcases, is to
define a definite minimalistic schema for the input file and create lots of files
using some sort of a command-line utility (because it can be scripted easily).

Conveniently there is a Python library just for that, to generate files in XML:
\begin{lstlisting}
pip install xmlgen
xmlgen -s schema.xml -o output.xml
xmlgen -r -o output.xml
\end{lstlisting}

\item \textbf{Use existing test-suites: }
There are multiple places where you can get random XML files originally used
for unittests and integration tests, which can also be used for fuzz testing.
Some websites I found were \href{https://xmltestsuite.sourceforge.net/}{xmltestsuite}



\item \textbf{Find random testcases:} Usually done by crawling the web or just
    cherry-picking potentially random documents.
\end{itemize}

In addition to corpus generation, including a \gls{dictionary} file helps the fuzzer learn
the grammar of the input files better. \href{https://github.com/AFLplusplus/AFLplusplus/blob/stable/dictionaries/xml.dict}{Here} is a standard \gls{dictionary} used for XML files.

\paragraph{Discarding testcases: } While discovering test cases is typically the
simplest aspect of fuzzing, discarding
them can become quite complex. It's crucial to maintain simplicity in test cases and
eliminate any that appear redundant. A useful starting point is the dry-run phase of
the fuzzer, where it flags potentially redundant test cases for further scrutiny.

AFL++ provides the \texttt{afl-cmin} tool to do exactly that.
\begin{lstlisting}
afl-cmin -i INPUTS -o INPUTS_UNIQUE -- bin/target -someopt @@
\end{lstlisting}

There is also \texttt{afl-tmin} which helps in reducing the size of the input files.

\subsubsection{Performance Optimizations}

The coverage of a fuzzer can be analysed using the following option:
\begin{lstlisting}
afl-showmap -C -i out -o /dev/null -- ./target -params @@
\end{lstlisting}

\paragraph{Assembling better corpus: }

If the corpus is large (either generated by fuzzing or acquired by other means)
minimization is possible while still preserving the full coverage. One way to do that
is to use the \texttt{-merge=1} flag in libfuzzer and \texttt{AUTO\_FILE} mode in AFL.

\paragraph{Experiment different Options: }

One of the common methods to improve the fuzzing is to use multi-threading and
use as many cores as possible.

Another neat trick is to use cache memory. In AFL for example the variable
\texttt{AFL\_TESTCACHE\_SIZE} can be set. Somewhere between \texttt{50-200 mb} is

\paragraph{Use custom Mutators:}

AFL++ supports the use of Custom Mutators.
Custom mutators can be passed to afl-fuzz to perform custom \glspl{mutation} on test cases beyond
those available in AFL. For example, to enable structure-aware fuzzing by using libraries
that perform \glspl{mutation} according to a given grammar.

Documentation for custom mutators can be found in the \href{https://github.com/AFLplusplus/AFLplusplus/blob/stable/docs/custom_mutators.md}{AFL++ docs}.

\subsubsection{Bug Discovery and Error Analysis}


Not all test cases that result in program crashes are necessarily indicative of
bugs. Given our reliance on stubs or modifications to the source code to test
required libraries, it's possible that such alterations themselves may lead to
crashes that are not reproducible in the full build.

This phase focuses on
distinguishing between bugs of this nature and those that warrant attention,
ensuring that only relevant issues are communicated to the project maintainers
via issue trackers.

It may also require us to include or exclude additional libraries to mitigate
these false-flags.







\subsection{Future Goals}

These goals may extend beyond the project's timeline and may require
authorization and administrative procedures from project leaders to make
decisions.

\subsubsection{CI integration}

As mentioned in earlier sections, CI integration is an important setup.
This enables short feedback cycles and makes it possible for developers to
quickly fix security vulnerabilities before they become a problem. But since
fuzzing can take very long, it can \textbf{block} the current flow. \cite{ci}

One approach to address this issue is by implementing defined periods
during which fuzzing activities are scheduled to occur. For instance, conducting
fuzzing after each software release or during weekends when the CI
is typically less occupied.

Another solution is to outsource fuzzing to a completely different service
like \texttt{oss-fuzz}. Doing so requires administrative permissions which are beyond my scope.
Once the current local fuzzers give promising results, it is reasonable to push
towards such decisions.

\subsubsection{Analysis Tools}

Once fuzzing discovers potential bugs, it becomes detrimental to
introspect the data and things like Bug Prioritization, Reproducibility
and categorization of errors come into play.

If we integrate with oss-fuzz, they offer a service called
\href{https://google.github.io/oss-fuzz/advanced-topics/fuzz-introspector/}{Fuzz
Introspect}.
It provides individual and aggregated fuzzer reachability and coverage reports.
You can monitor each fuzzer’s static reachability potential, compare it
against dynamic coverage, and identify any potential bottlenecks.


\begin{figure}[!ht]
    \center
\includegraphics[width=6in]{assets/introspect.png}
\caption{\href{https://github.com/ossf/fuzz-introspector/blob/main/doc/img/fuzz-introspector-architecture.png}{Source:}
Visualization of fuzz-introspector's workflow}
\end{figure}


\subsubsection{ML in fuzzing?}

While Machine Learning (ML) finds widespread application in various domains, its
utilization in fuzzing remains relatively limited. Theoretically, one could
employ different algorithms to train models for generating more effective
\glspl{mutation}, akin to a form of reinforcement learning. However, practical
implementation faces challenges due to the extensive number of features or
parameters involved, which currently fall beyond the scope of
feasibility.\cite{ml}

Fuzzing engines in general don't scale well for large and slow targets
(the best example is my trial fuzz). ML-based fuzzers can promise to solve the issue
by using massive-scale differential fuzzing.

Some projects that tried to implement this involve \texttt{Centipede}, but the
development has moved to Google's FuzzTesting repo as mentioned earlier.

\pagebreak

\section{Tentative Timeline}

\hrule \vspace{1em}
- Community bonding phase ( May 1 )
\paragraph{May 6 - May 26: }
\begin{itemize}
\item Get to know about the mentor and some specifics about the project.
\item Decide on the weekly meeting schedule and other processes.
\item Try making changes to the project plan to fit the needs.
\item Read up on the documentation about \texttt{XML parsing} and go through
more fuzzing tutorials
\item Decide on the primary fuzzer to use for the first iteration.
\end{itemize}
\hrule \vspace{1em}
- Coding Phase ( May 27 )

\paragraph{May 20 - June 10: } Instrument target
\begin{itemize}
\item Select the right compiler for fuzzing from various available ones
\item Decide with sanitizers to be used in each fuzzing campaign.
\item Modify the target, to remove unneeded libraries and functions that make fuzzing slower
\item Change the meson configuration to use the fuzz-compilers
\end{itemize}

\paragraph{June 10 - June 20: } Prepare the Corpus
\begin{itemize}
\item Collect initial XML inputs and try to include some interesting files.
\item Remove the redundant testcases that don't offer any uniqueness.
\item Minimize each of the corpus files to make the inputs as concise as possible
\end{itemize}

\paragraph{June 20 - July 11: } Optimize the fuzzer
\begin{itemize}
\item Reconfigure the system for optimal memory and multi-threading
\item Setup dictionaries and try out custom mutators.
\end{itemize}
\hrule \vspace{1em}
- Midterm Evaluations ( July 12 )

\paragraph{July 12 - August 10} Performance Analysis
\begin{itemize}
\item Monitor the status, check the coverage, paths discovered, etc.
\item Use commands such as \texttt{afl-showmap} to generate detailed reports
\end{itemize}
If the fuzzer is successfully executing and generating satisfactory outputs,
\begin{itemize}
\item Analyse the testcases, that cause errors
\item Try to categorize the given errors
\item Make issues on the issue-tracker for the valid ones
\end{itemize}
If the fuzzer gives performance issues and struggles to run
\begin{itemize}
\item Investigate the performance issues and possible source of slow-downs
\item If possible, seek help from fuzzing experts for solutions
\end{itemize}


\paragraph{August 10 - August 20: } Documentation Phase
(Though I will be documenting all these in my blog/repo, this is the official
documentation that I plan on adding to the website )
\begin{itemize}
\item Find one/many best fuzzing configurations that give satisfactory outputs
\item Write detailed documentation on the current setup
\item Document on the issues faces and reasons for the current choice of implementation
\end{itemize}
\hrule \vspace{1em}
- Final Evaluations ( August 26 )

\paragraph{August 20 - September 3 } Improve Portability
\begin{itemize}
\item Make scripts and builds to make the current configuration reproducible
\item Try using other secondary fuzzers and tweak the configuration to make it more general
\item Test the current setup in tools like docker or fresh VMs
\end{itemize}
\hrule \vspace{1em}
- Further Contributions

\paragraph{September 3 - ? }
\begin{itemize}
\item Work on the decided CI integration process
\item Decide on the analysis tools or try to make one
\end{itemize}




\pagebreak
\printnoidxglossary

\bibliographystyle{plainnat}
\bibliography{cite}
\pagebreak

\section*{Bio}

I am Abhiram Tilak, also recognized as "notpua" in IRC and some socials.
I am a sophomore pursuing a major in Computer Science at a
university in India, with a keen interest in conducting research specializing in
Particle Physics as a Computational Natural Scientist. Prior to college, I
lived in Dubai.

Outside of academics, I work as a student webadmin for my university and also
help around in system administration. I heavily rely on various open-source
projects like libvirt for self-hosting and managing my workflow. I have been a huge
open-source and privacy advocate since I was in high school.

\subsection*{Why GSoC?}

I have made some contributions to projects like \texttt{OhMyZsh} and
\texttt{Pyvista} but nothing serious as an independent project. Some
older contributions involve filing bug reports and docstrings
in projects like \texttt{Nixos} and \texttt{Wayland}. I also have some
interesting personal projects spanning from Operating systems to web-dev.

Participating in GSoC is an ideal opportunity to delve into projects
aligned with my interests and potentially aspire to become a maintainer of a
significant project. My ultimate career goal is to contribute to open-source
projects that I'm passionate about, and ideally, receive compensation and
decent pay similar to most of the Libvirt maintainers.

\subsection*{Availability during GSoC}

During the three-month summer break offered by my college to promote research
and internships, I am prepared to commit as a full-time developer to
\texttt{libvirt},
dedicating 30-40 hours per week to the project. However, once the summer
concludes, I will need to balance my commitments with semester studies and
assignments, which may reduce my availability to 15-20 hours per week.
Therefore, my goal is to complete the majority of the implementation by August.



\subsection*{Involvement after GSoC}

As for fuzzing, my primary goal is to advance libvirt to a stage where fuzzers
effectively uncover various bugs that can be promptly addressed.

While this
project doesn't entail building modules from scratch, which is beneficial for a
beginner like myself, I'm also eager to explore projects that involve more
complex development tasks.

Moreover, I've acquired proficiency in Rust last summer and have already started
incorporating it into various projects. I recall from an older KVM talk by
Daniel that there were discussions about replacing some core modules with Rust,
beyond the existing Rust bindings. I'm particularly enthusiastic about
contributing to such initiatives.



\end{document}
